import sys
def aplicar_filtro_sepia(imagen):
    width, height = imagen.size
    pix = imagen.load()

    for i in range(width):
        for j in range(height):
            r, g, b = imagen.getpixel((i, j))
            tr = int(0.393 * r + 0.769 * g + 0.189 * b)
            tg = int(0.349 * r + 0.686 * g + 0.168 * b)
            tb = int(0.272 * r + 0.534 * g + 0.131 * b)

            pix[i, j] = (min(tr, 255), min(tg, 255), min(tb, 255))

def aplicar_filtro_borrosidad(imagen, radio):
    # Aplicar el filtro de borrosidad
    imagen_borrosa = imagen.filter(ImageFilter.GaussianBlur(radius=radio))
    return imagen_borrosa

