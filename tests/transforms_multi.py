from transforms import change_colors, rotate_colors, rotate_right, mirror, blur, grayscale, filter, crop, shift
from images import read_img, write_img
import sys

def transform():
    if len(sys.argv) < 3 or len(sys.argv) % 2 == 0:
        print("Uso: python script.py <nombre_imagen> <funcion1> <args1> <funcion2> <args2> ...")
        sys.exit(1)

    try:
        pixels = read_img(sys.argv[1])
    except Exception as e:
        print(f"Error al abrir la imagen: {e}")
        sys.exit(1)

    # Obtener todas las funciones y argumentos
    funcs_args = sys.argv[2:]

    for i in range(0, len(funcs_args), 2):
        func = funcs_args[i].lower()  # Convertir a minúsculas para ser insensible a mayúsculas
        args = funcs_args[i + 1:]

        try:
            if func == "change_colors":
                pixels = change_colors(pixels, *args)
            elif func == "rotate_colors":
                pixels = rotate_colors(pixels, *args)
            elif func == "rotate_right":
                pixels = rotate_right(pixels)
            elif func == "mirror":
                pixels = mirror(pixels)
            elif func == "blur":
                pixels = blur(pixels, *args)
            elif func == "grayscale":
                pixels = grayscale(pixels)
            elif func == "filter":
                pixels = filter(pixels, *args)
            elif func == "crop":
                pixels = crop(pixels, *args)
            elif func == "shift":
                pixels = shift(pixels, *args)
            else:
                print(f"Función no reconocida: {func}")
                sys.exit(1)
        except Exception as e:
            print(f"Error al aplicar la función {func}: {e}")
            sys.exit(1)

    output_filename = f"{sys.argv[1].split('.')[0]}_trans.{sys.argv[1].split('.')[1]}"
    write_img(pixels, output_filename)
    print(f"Transformaciones aplicadas con éxito. Imagen guardada en '{output_filename}'")

def main():
    transform()

if __name__ == '__main__':
    main()