#ENTREGA CONVOCATORIA ENERO
Luciana Gironda Siña l.gironda.2023@alumnos.urjc.es
En esta entrega se incluyen los siguientes archivos:
- Enlace al video: https://youtu.be/QyJeYE6bOjs?si=hyQYcR9Z8k2OWkux
- Apartado requisitos minimos:
  1.metodo change_colors
  2.metodo rotate_right
  3.metodo mirror
  4.metodo rotate_colors
  5.metodo blur
  6.metodo shift
  7.metodo crop
  8.metodo greyescale
  9.metodo filter
  10.metodo transforms_simple
  11.metodo transforms_multi
  12.metodo transforms_args
- Apartado funciones avanzadas:
  
  Metodo sepia: 
  Esta función toma una imagen (representada por el objeto imagen de Pillow) y aplica un filtro sepia a cada píxel. El bucle for itera sobre cada píxel de la imagen y calcula nuevos valores de componentes de color (tr, tg, tb) utilizando una combinación lineal de los valores originales (r, g, b). Luego, se asegura de que los nuevos valores estén en el rango [0, 255] utilizando la función min. Finalmente, se asignan los nuevos valores de color al píxel correspondiente.
  
  Metodo Borrosidad:

  El método de borrosidad, en este caso, se implementa utilizando un filtro de suavizado, también conocido como filtro de borrosidad gaussiana. El filtro de borrosidad gaussiana se basa en la convolución de la imagen original con un kernel gaussiano. La convolución es un proceso matemático que combina dos funciones para crear una tercera. En este contexto, el kernel gaussiano actúa como una función de ponderación que se aplica a cada píxel de la imagen original para obtener un nuevo valor.
  La función GaussianBlur de la biblioteca Pillow (PIL) implementa este filtro. La idea detrás del filtro gaussiano es suavizar la imagen al dar más peso a los píxeles cercanos y menos peso a los píxeles más alejados. Esto tiene el efecto de reducir las transiciones abruptas de color, lo que resulta en una imagen borrosa.

